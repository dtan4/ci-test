require "docker"
require "serverspec"

set :backend, :docker
set :docker_url, ENV["DOCKER_HOST"]

if ENV['QUAY_IO_TRIGGER_TAG'].nil? || ENV['QUAY_IO_TRIGGER_TAG'].empty?
  set :docker_image, "quay.io/abeja/ci-test"
else
  set :docker_image, "quay.io/abeja/ci-test:#{ENV['QUAY_IO_TRIGGER_TAG']}"
end
